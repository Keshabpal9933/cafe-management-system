global.$ = global.jQuery = require('jquery');
require("../templet/home/vendor/animsition/js/animsition");
require("bootstrap");
require("../templet/home/vendor/slick/slick");
require("../templet/home/js/slick-custom");
require("../templet/home/vendor/parallax100/parallax100");
$('.parallax100').parallax100();
require("../templet/home/vendor/MagnificPopup/jquery.magnific-popup");
$('.gallery-lb').each(function() { // the containers for all your galleries
    $(this).magnificPopup({
        delegate: 'a', // the selector for gallery item
        type: 'image',
        gallery: {
            enabled:true
        },
        mainClass: 'mfp-fade'
    });
});
require("../templet/home/vendor/isotope/isotope.pkgd.min");
require("../templet/home/vendor/sweetalert/sweetalert.min");

require("../templet/home/js/main");