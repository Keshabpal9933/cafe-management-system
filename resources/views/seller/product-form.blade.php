@extends('layouts.seller')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/summernote-bs4.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('js/summernote-bs4.js') }}"></script>

    {{-- for summer-note --}}
    <script>
        $('#description').summernote({
            height:150
        });

        {{-- for show sub-category in the product-form --}}
        $('#cat_id').change(function () {
           var cat_id = $('#cat_id').val();
           var sub_cat_id = "{{ (isset($product_data)) ? $product_data->sub_cat_id : null }}";
            if(cat_id){
                $.ajax({
                    url: "{{ route('get-child') }}",
                    type: "post",
                    data: {
                        _token: "{{ csrf_token() }}",
                        cat_id: cat_id
                    },
                    success:function (response) {
                        if(typeof(response) != 'object'){
                            response = JSON.parse(response);
                        }

                        var html_option = "<option value='' selected > -- Select Any One -- </option>";
                        if(response.status){
                            $.each(response.data, function (key,value) {
                                html_option += "<option value='"+value.id+"' ";

                                if(value.id == sub_cat_id){
                                    html_option += 'selected';
                                }
                                html_option += " > "+value.title+" </option>";
                            });
                            $('#sub_cat_div').removeClass('d-none');
                        }else{
                            $('#sub_cat_div').addClass('d-none');
                        }

                        $('#sub_cat_id').html(html_option);

                    }
                });
            }
        });

        $('#cat_id').change();
    </script>

@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Product {{ isset($product_data) ? 'Update' : 'Add' }} Form</div>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-body">

                    @if(isset($product_data))
                        {{ Form::open(['url'=>route('products.update', $product_data->id),'class'=>'form','files'=>true]) }}
                        @method('put')
                    @else
                        {{ Form::open(['url'=>route('products.store'),'class'=>'form','files'=>true]) }}
                    @endif

                    <div class="form-group row">
                        {{ Form::label('title','Title',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::text('title',@$product_data->title,['class'=>'form-control form-control-sm','required'=>true,'id'=>'title','placeholder'=>'Enter title']) }}
                            @error('title')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('summary','Summary',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::textarea('summary',@$product_data->summary,['class'=>'form-control form-control-sm','required'=>true,'id'=>'summary','placeholder'=>'Enter summary','rows'=>5,'style'=>'resize:none']) }}
                            @error('summary')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('description','Description',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::textarea('description',@$product_data->description,['class'=>'form-control form-control-sm','required'=>false,'id'=>'description','placeholder'=>'Enter description','rows'=>5,'style'=>'resize:none']) }}
                            @error('description')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        {{ Form::label('cat_id','Category',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::select('cat_id',$parent_data,@$product_data->cat_id,['class'=>'form-control form-control-sm','required'=>true,'id'=>'cat_id','placeholder'=>'---Select any one---']) }}
                            @error('cat_id')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row d-none " id="sub_cat_div">
                        {{ Form::label('sub_cat_id','Sub-Category',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::select('sub_cat_id',$parent_data,@$product_data->sub_cat_id,['class'=>'form-control form-control-sm','id'=>'sub_cat_id','placeholder'=>'---Select any one---']) }}
                            @error('sub_cat_id')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('price','Price(NPR. )',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::text('price',@$product_data->price,['class'=>'form-control form-control-sm','required'=>true,'id'=>'price','placeholder'=>'Enter price','min'=>0]) }}
                            @error('price')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('discount','Discount(% )',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::text('discount',@$product_data->discount,['class'=>'form-control form-control-sm','required'=>false,'id'=>'discount','placeholder'=>'Enter discount','min'=>0,'max'=>100]) }}
                            @error('discount')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('delivery_charge','Delivery Charge',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::text('delivery_charge',@$product_data->delivery_charge,['class'=>'form-control form-control-sm','required'=>true,'id'=>'delivery_charge','placeholder'=>'Enter delivery_charge']) }}
                            @error('delivery_charge')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        {{ Form::label('is_featured','Is Featured ?',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::checkbox('is_featured','1',isset($product_data, $product_data->is_featured) && $product_data->is_featured != null ? false : true,['id'=>'is_featured']) }}
                            Yes
                        </div>
                    </div>



                    <div class="form-group row">
                        {{ Form::label('image','Image',['class'=>'col-sm-3']) }}
                        <div class="col-sm-4">
                            {{ Form::file('image',['id'=>'image','accept'=>'image/*','required'=>(isset($product_data) ? false : true)]) }}
                            @error('image')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="col-sm-4">
                            @if(isset($product_data))
                                <img src="{{ imageUrl($product_data->image, 'product') }}" alt=""
                                     style="max-width: 100px;" class="img img-fluid img-thumbnail">
                            @endif
                        </div>

                    </div>


                    <div class="form-group row">
                        {{ Form::label('related_image','Related_Image',['class'=>'col-sm-3']) }}
                        <div class="col-sm-4">
                            {{ Form::file('rel_image[]',['accept'=>'image/*','multiple'=>true]) }}
                            @error('related_image')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                        {{-- for related image --}}
                        @if(isset($product_data->images))

                            <div class="form-group row">
                                @foreach($product_data->images as $rel_image)
                                    <div class="col-sm-3">
                                        <img src="{{ asset('uploads/product/Thumb-'.$rel_image->image_name) }}" alt="" class="img img-fluid img-thumbnail">
                                        {{ Form:: checkbox('del_image[]',$rel_image->image_name, false) }} Delete
                                    </div>
                                @endforeach
                            </div>

                        @endif


                    <div class="form-group row">
                        <div class="col-sm-9 offset-sm-3">
                            {{ Form::button('<i class="fa fa-trash"></i> Re-set',['class'=>'btn btn-danger btn-sm','id'=>'reset','type'=>'reset']) }}
                            {{ Form::button('<i class="fa fa-paper-plane"></i> Submit',['class'=>'btn btn-success btn-sm','id'=>'submit','type'=>'submit']) }}
                        </div>
                    </div>


                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection

