<div class="p-b-10">
    <h3 class="ltext-103 cl5">
        Product Overview
    </h3>
</div>

<div class="flex-w flex-sb-m p-b-52">
    <div class="flex-w flex-l-m filter-tope-group m-tb-10">

        <button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5 how-active1" data-filter="*">
            All Products
        </button>

        @if($category)
            @foreach($category as $filter_cat)
                <button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5" data-filter=".{{ $filter_cat->id }}">
                    {{ $filter_cat->title }}
                </button>
            @endforeach
        @endif

    </div>
</div>
