<!-- Back to top -->
<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="zmdi zmdi-chevron-up"></i>
		</span>
	</div>

    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

<script>
    $('.js-addcart-detail').click(function () {
        let product_id = $(this).data('product_id');
        let quantity = $('#quantity').val();

        // alert(product_id+" "+quantity);

        if(quantity >= 0){
            addToCart(product_id, quantity);
        }

    });

    $('.btn_cart_update').click(function () {
		let product_id = $(this).data('id');
		let quantity = $(this).data('quantity');
		addToCart(product_id, quantity);

	});

    function addToCart(product_id, quantity) {
        $.ajax({
            url: "{{ route('add-to-cart') }}",
            type: "post",
            data: {
                _token: "{{ csrf_token() }}",
                prod_id: product_id,
                qty:quantity
            },
            success: function (response) {
                // console.log(response)

                if(typeof(response) != "object"){
                    response = JSON.parse(response);
                }

                                                                            //for update the session, for cart update
                                                                            //for page reload, after page reload only session will updated
                if(response){
                    swal("Update!",response.msg,'success').then(function(){
                        document.location.href = document.location.href;
                    });
                }else{
                    swal("Sorry!",response.msg,'error');
                }

            }
        });
    }


</script>

@yield('scripts')

</body>
</html>
