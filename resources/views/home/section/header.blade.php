<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="csrf-token" content="{{ csrf_token() }}" >
	<title>@yield('title')</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	@yield('meta')       <!-- for page wise SEO    this meta is defined in welcome.blade.php or any other page -->
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{ asset('images/icons/favicon.png') }}"/>
<!--===============================================================================================-->

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

	@yield('styles')

</head>
<body class="animsition">