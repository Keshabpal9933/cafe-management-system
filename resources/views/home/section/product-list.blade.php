<div class="row isotope-grid">

    @if(isset($product_all))
        @foreach($product_all as $all_products)
            <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item {{ $all_products->cat_id }}">
                <!-- Block2 -->
                <div class="block2">
                    <div class="block2-pic hov-img0">
                        <img src="{{ imageUrl($all_products->image, 'product') }}" style="height: 350px" alt="IMG-PRODUCT">

                        <a href="#" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                            Quick View
                        </a>
                    </div>

                    <div class="block2-txt flex-w flex-t p-t-14">
                        <div class="block2-txt-child1 flex-col-l ">
                            <a href="{{ route('product-detail',$all_products->slug) }}" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                               {{ $all_products->title }}
                            </a>

                            <span class="stext-105 cl3">
									NPR. {{ number_format($all_products->actual_cost) }}

                                @if($all_products->discount > 0)
                                    <span style="color: #0f4bac">{{ $all_products->discount }}% Dis. </span>
                                    <del style="color: #ff0000">
                                        NPR. {{ number_format($all_products->price) }}
                                    </del>

                                    @endif

							</span>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
        @else
        <p>No Product Found</p>
    @endif


</div>
