<!-- Header -->
<header class="header-v4">
		<!-- Header desktop -->
		<div class="container-menu-desktop">
			<!-- Topbar -->
			<div class="top-bar">
				<div class="content-topbar flex-sb-m h-full container">
					<div class="left-top-bar">
					</div>
					<div class="right-top-bar flex-w h-full">

                    @auth()
                            <a href="{{ route(auth()->user()->role) }}" class="flex-c-m trans-04 p-lr-25">
                                {{ auth()->user()->name }}
                            </a>

                            <a href="javascript:;" onclick="event.preventDefault(); document.getElementById('logout-form').submit(); confirm('Are you sure you want to logout')"  class="flex-c-m trans-04 p-lr-25">
                                   <i class="fa fa-power-off"></i>  LogOut
                            </a>
                         {{Form::open(['url'=>route('logout'), 'id'=>'logout-form'])}}
                        {{Form::close()}}


                        @else
                            <a href="{{ route('login') }}" class="flex-c-m trans-04 p-lr-25">
                                Log-In
                            </a>
                            <a href="{{ route('login') }}" class="flex-c-m trans-04 p-lr-25">
                                Register
                            </a>
                     @endauth

                    </div>
				</div>
			</div>

			<div class="wrap-menu-desktop">
				<nav class="limiter-menu-desktop container">

					<!-- Logo desktop -->
					<a href="{{ route('homepage') }}" class="logo">
						<img src="{{ asset('images/icons/logo.png') }}" alt="IMG-LOGO">
					</a>

					<!-- Menu desktop -->
					<div class="menu-desktop">
						<ul class="main-menu">
							<li class="active-menu">
								<a href="{{ route('homepage') }}">Home</a>
							</li>

							<li>
								<a href="{{ route('product-list') }}">All Items</a>
							</li>


                            {{ getHeaderMenu() }}

							<li class="label1" {{--data-label1="hot"--}}>
								<a href="{{ route('featured-products') }}">Featured Menu Items</a>
							</li>
						</ul>
					</div>

					<!-- Icon header -->
					<div class="wrap-icon-header flex-w flex-r-m">

                        @php
                            $total_prod = 0;

                            if(session('cart')){
                                foreach (session('cart') as $cart_items){
                                    $total_prod += $cart_items['quantity'];
                                }
                            }
                        @endphp

						<div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart" data-notify="{{ $total_prod }}">
							<i class="zmdi zmdi-shopping-cart"></i>
						</div>

					</div>
				</nav>
			</div>
		</div>
	</header>
