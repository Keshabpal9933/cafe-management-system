@extends('layouts.app')

@section('title','Home | Cafe Management Portal')

@section('meta')
    <meta name="keywords" content="" >
    <meta name="description" content="" >

    <meta name="og:type" content="" >
    <meta property="og:url" content="" >
    <meta property="og:title" content="" >
    <meta property="og:description" content="" >
    <meta property="og:image" content="" >
@endsection

@section('content')

    <!-- Slider -->
    @if($slider)
        <section class="section-slide">
            <div class="wrap-slick1">
                <div class="slick1">

                    @foreach($slider as $slider_data)
                        <div class="item-slick1">
                            <div class="container-fluid h-full">
                                <a href="{{ $slider_data->link }}" target="_slider">
                                    <img src="{{ imageUrl($slider_data->image, 'slider') }}" alt="" style="width: 100%" class="img">
                                </a>
                            </div>
                        </div>
                     @endforeach

                </div>
            </div>
        </section>
    @endif


    <!-- Banner -->
    <div class="sec-banner bg0 p-t-80 p-b-50">
        <div class="container">
            <div class="row">

                @if($category)
                    @foreach($category as $key => $home_tab)
                        <div class="col-md-6 col-xl-3 p-b-30 m-lr-auto">
                            <!-- Block1 -->
                            <div class="block1 wrap-pic-w">
                                <img src="{{ imageUrl($home_tab->image,'category') }}" style="width: 250px; height: 200px;" alt="IMG-BANNER">

                                <a href="{{ route('category-products',$home_tab->slug) }}" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                    <div class="block1-txt-child1 flex-col-l">
                                        <span class="block1-name ltext-102 trans-04 p-b-8">
                                           {{ $home_tab->title }}
                                           hello
                                        </span>
                                    </div>
                                </a>
                            </div>
                        </div>
                        @if($key == 3)     {{-- for fetch only 8 category name listed --}}
                            @break
                        @endif
                    @endforeach
                @endif

            </div>
        </div>
    </div>


    <!-- Product -->
    <section class="bg0 p-t-23 p-b-140">
        <div class="container">
            @include('home.section.filter')

            @include('home.section.product-list')

            <!-- Load more -->
                <div class="flex-c-m flex-w w-full p-t-45">
                   {{-- <a href="{{route('')}}" class="flex-c-m stext-101 cl5 size-103 bg2 bor1 hov-btn1 p-lr-15 trans-04">
                        Load More
                    </a>--}}
                    <button type="button" name="load_more_button" class="btn btn-success form-control" data-id="'.$last_id.'" id="load_more_button">Load More</button>
                </div>
        </div>
    </section>

    <script>
        $(document).ready(function(){

            var _token = $('input[name="_token"]').val();

            load_data('', _token);

            function load_data(id="", _token)
            {
                $.ajax({
                    url:"{{ route('loadmore.load_data') }}",
                    method:"POST",
                    data:{id:id, _token:_token},
                    success:function(data)
                    {
                        $('#load_more_button').remove();
                        $('#post_data').append(data);
                    }
                })
            }

            $(document).on('click', '#load_more_button', function(){
                var id = $(this).data('id');
                $('#load_more_button').html('<b>Loading...</b>');
                load_data(id, _token);
            });

        });
    </script>

@endsection
