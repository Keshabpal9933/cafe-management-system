@extends('layouts.app')
@section('styles')
    <style>
        .wrap-slick3-dots{
            height: 500px;
            overflow: scroll;
        }
    </style>
@endsection
@section('meta')

@endsection

@section('content')

    <!-- breadcrumb -->
    <div class="container">
        <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
            <a href="{{ route('homepage') }}" class="stext-109 cl8 hov-cl1 trans-04">
                Home
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <a href="{{ route('category-products',$product_detail->cat_info['slug']) }}"
               class="stext-109 cl8 hov-cl1 trans-04">
                {{ $product_detail->cat_info['title'] }}
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            @if($product_detail->sub_cat_info)
                <a href="{{ route('child-category-products',[$product_detail->cat_info['slug'],$product_detail->sub_cat_info['slug']]) }}"
                   class="stext-109 cl8 hov-cl1 trans-04">
                    {{ $product_detail->sub_cat_info['title'] }}
                    <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
                </a>
            @endif

            <span class="stext-109 cl4">
				{{ $product_detail->title }}
			</span>
        </div>
    </div>


    <!--**************************************** Product Detail ****************************************-->
    <section class="sec-product-detail bg0 p-t-65 p-b-60">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-7 p-b-30">
                    <div class="p-l-25 p-r-30 p-lr-0-lg">
                        <div class="wrap-slick3 flex-sb flex-w">
                            <div class="wrap-slick3-dots"></div>
                            <div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

                            <div class="slick3 gallery-lb">
                                @if($product_detail->images)
                                    @foreach($product_detail->images as $prod_images)
                                        <div class="item-slick3"
                                             data-thumb="{{ imageUrl($prod_images->image_name,'product') }}">
                                            <div class="wrap-pic-w pos-relative">
                                                <img src="{{ imageUrl($prod_images->image_name,'product') }}"
                                                     alt="IMG-PRODUCT">

                                                <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04"
                                                   href="{{ asset('uploads/product/'.$prod_images->image_name) }}">
                                                    <i class="fa fa-expand"></i>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-5 p-b-30">
                    <div class="p-r-50 p-t-5 p-lr-0-lg">
                        <h4 class="mtext-105 cl2 js-name-detail p-b-14">
                            {{ $product_detail->title }}
                        </h4>

                        <span class="mtext-106 cl2">
						NPR. {{ number_format($product_detail->actual_cost) }}
                            @if($product_detail->discount > 0)
                                <del style="color: #ff0000">{{ $product_detail->price }}</del>
                            @endif
						</span>


                        <p class="stext-102 cl3 p-t-23">
                            {{ $product_detail->summary }}
                        </p>

                  {{--********************************* Add to Cart *********************************--}}
                            <div class="flex-w flex-r-m p-b-10">
                                <div class="size-204 flex-w flex-m respon6-next">
                                    <div class="wrap-num-product flex-w m-r-20 m-tb-10">
                                        <div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
                                            <i class="fs-16 zmdi zmdi-minus"></i>
                                        </div>

                                        <input id="quantity" class="mtext-104 cl3 txt-center num-product" type="number"
                                               name="num-product" value="{{ $quantity }}">

                                        <div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
                                            <i class="fs-16 zmdi zmdi-plus"></i>
                                        </div>
                                    </div>

                                    <button
                                       data-product_id="{{ $product_detail->id }}" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 js-addcart-detail">
                                        Add to cart
                                    </button>
                                </div>
                            </div>
                        </div>

                        <!--  -->

                    </div>
                </div>
            </div>

            <div class="bor10 m-t-50 p-t-43 p-b-40">
                <!-- Tab01 -->
                <div class="tab01">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item p-b-10">
                            <a class="nav-link active" data-toggle="tab" href="#description" role="tab">Description</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content p-t-43">
                        <!-- - -->
                        <div class="tab-pane fade show active" id="description" role="tabpanel">
                            <div class="how-pos2 p-lr-15-md">
                                <div class="stext-102 cl6">
                                    {!! $product_detail->description !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>


    <!--*************************** Related Products ***************************-->
    <section class="sec-relate-product bg0 p-t-45 p-b-105">
        <div class="container">
            <div class="p-b-45">
                <h3 class="ltext-106 cl5 txt-center">
                    Related Products
                </h3>
            </div>

            <!-- Slide2 -->
            <div class="wrap-slick2">
                <div class="slick2">

                    @if($product_detail->related_products)
                        @foreach($product_detail->related_products as $product_related)
                            <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                                <!-- Block2 -->
                                <div class="block2">
                                    <div class="block2-pic hov-img0">
                                        <img src="{{ imageUrl($product_related->image,'product') }}" alt="IMG-PRODUCT">

                                        <a href="#"
                                           class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                            Quick View
                                        </a>
                                    </div>

                                    <div class="block2-txt flex-w flex-t p-t-14">
                                        <div class="block2-txt-child1 flex-col-l ">
                                            <a href="{{ route('product-detail',$product_related->slug) }}"
                                               class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                {{ $product_related->title }}
                                            </a>

                                            <span class="stext-105 cl3">
										        NPR. {{ number_format($product_related->actual_cost) }}

                                                @if($product_related->discount > 0)
                                                    <del
                                                        style="color: #ff0000">{{ number_format($product_related->price) }}</del>
                                                @endif
									        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </section>

@endsection
