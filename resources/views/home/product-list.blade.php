@extends('layouts.app')

@section('content')
    <!-- Product -->
    <section class="bg0 p-t-23 p-b-140">
        <div class="container">
        @include('home.section.filter')

        @include('home.section.product-list')


            {{ $product_all->links() }}
        </div>
    </section>

@endsection
