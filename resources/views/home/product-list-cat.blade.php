@extends('layouts.app')

@section('content')
    <!-- Product -->
    <section class="bg0 p-t-23 p-b-140">
        <div class="container">
            <div class="p-b-10">
                <h3 class="ltext-103 cl5">
                    {{ $cat_info->title }}
                </h3>
            </div>

        @include('home.section.product-list')

        </div>
    </section>

@endsection
