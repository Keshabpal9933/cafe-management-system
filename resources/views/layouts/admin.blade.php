@include('admin.section.top-link')
<div class="page-wrapper">

    <!-- START HEADER-->
    @include('admin.section.header')
    <!-- END HEADER-->

    @include('admin.section.sidebar')

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-content fade-in-up">
            @include('admin.section.notify')
            @yield('content')
        </div>

    </div>
</div>

@include('admin.section.footer')
