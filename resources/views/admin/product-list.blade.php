@extends('layouts.admin')

@section('scripts')
    <script>
        $('.delete_btn').click(function (e) {
            var id= $(this).data('id');
            var confirmed = confirm('Are you sure you want to delete the product data ?');
            if(confirmed){
                $('#'+id).submit();
            }
        });
    </script>

@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Product List</div>
                    <div class="ibox-tools">
                        <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                        <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item">option 1</a>
                            <a class="dropdown-item">option 2</a>
                        </div>
                    </div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Price</th>
                        <th>Is Featured</th>
                        <th>Seller</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @if($product_data)

                            @foreach($product_data as $product_info)

                                <tr>
                                    <td>{{ $product_info->title }}</td>

                                    <td>
                                        {{ $product_info->cat_info['title'] }}
                                        <sub>
                                            {{ $product_info->sub_cat_info['title']}}
                                        </sub>
                                    </td>

                                    <td> NPR. {{ number_format($product_info->actual_cost) }} </td>

                                    <td> {{ ($product_info->is_featured == 1 ) ? 'Yes' : 'No' }} </td>

                                    <td>{{ $product_info->seller_info['name'] }}</td>

                                    <td>
                                        <img src="{{ imageUrl($product_info->image, 'product') }}" alt="" style="max-width: 100px;" class="img img-fluid img-thumbnail">
                                    </td>


                                    <td>
                                            <span class="badge badge-{{ $product_info->status == 'active' ? 'success' : 'danger' }}">
                                                {{ ucfirst($product_info->status) }}
                                            </span>
                                    </td>

                                    <td>
                                        <a href="{{ route('product.edit', $product_info->id) }}" class="btn btn-success btn-sm">
                                            <i class="fa fa-edit"></i>
                                        </a>


                                        <a href="javascript:;" class="btn btn-danger btn-sm delete_btn" data-id="product-{{ $product_info->id }}">
                                            <i class="fa fa-trash"></i>
                                        </a>

                                        {{ Form::open(['url'=>route('product.destroy', $product_info->id), 'id'=>'product-'.$product_info->id]) }}
                                        @method('delete')
                                        {{ Form::close() }}



                                    </td>

                                </tr>

                            @endforeach

                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
