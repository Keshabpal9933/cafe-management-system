@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Page List</div>
                    <div class="ibox-tools">
                        <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                        <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item">option 1</a>
                            <a class="dropdown-item">option 2</a>
                        </div>
                    </div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <th>Title</th>
                        <th>Summary</th>
                        <th>Image</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @if($page_data)

                            @foreach($page_data as $page_info)

                                <tr>
                                    <td>{{ $page_info->title }}</td>
                                    <td>{{ $page_info->summary }}</td>

                                   

                                    <td>
                                        <img src="{{ imageUrl($page_info->image, 'pages') }}" alt="" style="max-width: 100px;" class="img img-fluid img-thumbnail">
                                    </td>


                                    <td>
                                        <a href="{{ route('pages.edit', $page_info->id) }}" class="btn btn-success btn-sm">
                                            <i class="fa fa-edit"></i>
                                        </a>

                                    </td>

                                </tr>

                            @endforeach

                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
