@extends('layouts.admin')



@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Slider {{ isset($slider_data) ? 'Update' : 'Add' }} Form</div>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-body">

                    @if(isset($slider_data))
                        {{ Form::open(['url'=>route('slider.update', $slider_data->id),'class'=>'form','files'=>true]) }}
                        @method('put')
                    @else
                        {{ Form::open(['url'=>route('slider.store'),'class'=>'form','files'=>true]) }}
                    @endif

                    <div class="form-group row">
                        {{ Form::label('title','Title',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::text('title',@$slider_data->title,['class'=>'form-control form-control-sm','required'=>true,'id'=>'title','placeholder'=>'Enter title']) }}
                            @error('title')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        {{ Form::label('link','Link',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::url('link',@$slider_data->link,['class'=>'form-control form-control-sm','required'=>false,'id'=>'link','placeholder'=>'Enter link']) }}
                            @error('link')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        {{ Form::label('status','Status',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::select('status',['active'=>'Active','inactive'=>'Inactive'],@$slider_data->status,['class'=>'form-control form-control-sm','required'=>true]) }}
                            @error('status')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        {{ Form::label('image','Image',['class'=>'col-sm-3']) }}
                        <div class="col-sm-4">
                            {{ Form::file('image',['required'=>(isset($slider_data) ? false : true),'id'=>'image','accept'=>'image/*']) }}
                            @error('image')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="col-sm-4">
                            @if(isset($slider_data))

                                <img src="{{ imageUrl($slider_data->image, 'slider') }}" alt="" style="max-width: 100px;" class="img img-fluid img-thumbnail">

                            @endif
                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-sm-9 offset-sm-3">
                            {{ Form::button('<i class="fa fa-trash"></i> Re-set',['class'=>'btn btn-danger btn-sm','id'=>'reset','type'=>'reset']) }}
                            {{ Form::button('<i class="fa fa-paper-plane"></i> Submit',['class'=>'btn btn-success btn-sm','id'=>'submit','type'=>'submit']) }}
                        </div>
                    </div>



                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection
