@extends('layouts.admin')

@section('scripts')
    <script>
        $('.delete_btn').click(function (e) {
            var id= $(this).data('id');
            var confirmed = confirm('Are you sure you want to delete the category data ?');
            if(confirmed){
                $('#'+id).submit();
            }
        });
    </script>

@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Category List</div>
                    <div class="ibox-tools">
                        <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                        <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item">option 1</a>
                            <a class="dropdown-item">option 2</a>
                        </div>
                    </div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <th>Title</th>
                        <th>Sub Category</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @if($category_data)

                            @foreach($category_data as $category_info)

                                <tr>
                                    <td>{{ $category_info->title }}</td>

                                    <td>
                                        @foreach($category_info->child_cats as  $child_category)
                                            <div class="row border-bottom">
                                                <div class="col-sm-6">
                                                    <a href="{{ route('category.edit',$child_category->id) }}" class="btn btn-link">
                                                        {{ $child_category->title }}
                                                    </a>
                                                </div>

                                                <div class="col-sm-6">
                                                    {{ Form::open(['url'=>route('category.destroy', $child_category->id),'onsubmit'=>'return confirm("Are you sure you want to delete this category.")']) }}
                                                    @method('delete')
                                                    {{ Form::button("<i class='fa fa-trash'></i>",['type'=>'submit','class'=>'btn btn-light btn-sm float-auto','style'=>'color:red;']) }}
                                                    {{ Form::close() }}
                                                </div>
                                            </div>
                                        @endforeach
                                    </td>

                                    <td>
                                        <img src="{{ imageUrl($category_info->image, 'category') }}" alt="" style="max-width: 100px;" class="img img-fluid img-thumbnail">
                                    </td>


                                    <td>
                                            <span class="badge badge-{{ $category_info->status == 'active' ? 'success' : 'danger' }}">
                                                {{ ucfirst($category_info->status) }}
                                            </span>
                                    </td>

                                    <td>
                                        <a href="{{ route('category.edit', $category_info->id) }}" class="btn btn-success btn-sm">
                                            <i class="fa fa-edit"></i>
                                        </a>


                                        <a href="javascript:;" class="btn btn-danger btn-sm delete_btn" data-id="category-{{ $category_info->id }}">
                                            <i class="fa fa-trash"></i>
                                        </a>

                                        {{ Form::open(['url'=>route('category.destroy', $category_info->id), 'id'=>'category-'.$category_info->id]) }}
                                        @method('delete')
                                        {{ Form::close() }}



                                    </td>

                                </tr>

                            @endforeach

                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
