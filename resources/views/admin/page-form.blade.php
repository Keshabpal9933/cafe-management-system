@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/summernote-bs4.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('js/summernote-bs4.js') }}"></script>

    {{-- for summer-note --}}
    <script>
        $('#description').summernote({
            height:150
        });

        </script>
@endsection


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Page Update Form</div>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-body">

                        {{ Form::open(['url'=>route('pages.update', $page_data->id),'class'=>'form','files'=>true]) }}
                        @method('put')

                    <div class="form-group row">
                        {{ Form::label('title','Title',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::text('title',@$page_data->title,['class'=>'form-control form-control-sm','required'=>true,'id'=>'title','placeholder'=>'Enter title']) }}
                            @error('title')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        {{ Form::label('summary','Summary',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::textarea('summary',@$page_data->summary,['class'=>'form-control form-control-sm','required'=>true,'id'=>'summary','placeholder'=>'Enter summary','rows'=>5,'style'=>'resize:none']) }}
                            @error('summary')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('description','Description',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::textarea('description',@$page_data->description,['class'=>'form-control form-control-sm','required'=>false,'id'=>'description','placeholder'=>'Enter description','rows'=>5,'style'=>'resize:none']) }}
                            @error('description')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                        
                        <div class="form-group row">
                            {{ Form::label('image','Image',['class'=>'col-sm-3']) }}
                            <div class="col-sm-4">
                                {{ Form::file('image',['id'=>'image','accept'=>'image/*']) }}
                                @error('image')
                                <span class="span-danger">{{ $message }}</span>
                                @enderror
                            </div>

                        <div class="col-sm-4">
                            @if(isset($page_data))

                                <img src="{{ imageUrl($page_data->image, 'pages') }}" alt="" style="max-width: 100px;" class="img img-fluid img-thumbnail">

                            @endif
                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-sm-9 offset-sm-3">
                            {{ Form::button('<i class="fa fa-trash"></i> Re-set',['class'=>'btn btn-danger btn-sm','id'=>'reset','type'=>'reset']) }}
                            {{ Form::button('<i class="fa fa-paper-plane"></i> Submit',['class'=>'btn btn-success btn-sm','id'=>'submit','type'=>'submit']) }}
                        </div>
                    </div>



                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection

