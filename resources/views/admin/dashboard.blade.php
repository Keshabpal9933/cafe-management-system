@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="ibox bg-success color-white widget-stat">
                <div class="ibox-body">
                    <h2 class="m-b-5 font-strong">{{ $order }}</h2>
                    <div class="m-b-5">NEW ORDERS</div><i class="ti-shopping-cart widget-stat-icon"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="ibox bg-info color-white widget-stat">
                <div class="ibox-body">
                    <h2 class="m-b-5 font-strong">{{ $user }}</h2>
                    <div class="m-b-5">TOTAL USERS</div><i class="fa fa-users widget-stat-icon"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="ibox bg-warning color-white widget-stat">
                <div class="ibox-body">
                    <h2 class="m-b-5 font-strong">NPR.0</h2>
                    <div class="m-b-5">TOTAL INCOME</div><i class="fa fa-money-bill widget-stat-icon"></i>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="ibox bg-warning color-white widget-stat">
                <div class="ibox-body">
                    <h2 class="m-b-5 font-strong">{{ $product }}</h2>
                    <div class="m-b-5">TOTAL PRODUCT</div><i class="fa fa-shopping-bag widget-stat-icon"></i>
                </div>
            </div>
        </div>


    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Latest Orders</div>
                    {{--<div class="ibox-tools">
                        <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                        <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item">option 1</a>
                            <a class="dropdown-item">option 2</a>
                        </div>
                    </div>--}}
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Customer</th>
                            <th>Menu-Item</th>
                            <th>Quantity/Price</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th width="91px">Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @if(isset($cart))
                         @foreach($cart as $value)
                        <tr>
                            <td>
                                <a href="#">{{$value->cart_code}}</a>
                            </td>
                            <td>{{$value->user_info['name']}}</td>
                            <td>{{ $value->product_info['title'] }}</td>
                            <td>{{$value->quantity}}x{{$value->actual_price}}</td>
                            <td>NPR. {{number_format($value->total_amount)}}</td>
                            <td>
                                <span class="badge badge-success">{{$value->status}}</span>
                            </td>
                            <td>{{$value->created_at}}</td>
                            <td>
                                {{--<a href="{{route('message.create')}}">--}}
                                    <i class="fas fa-sign-out-alt fa-3x"></i>

                            </td>
                        </tr>
                        @endforeach
                       @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
