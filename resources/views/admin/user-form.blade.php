@extends('layouts.admin')

@section('scripts')

@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">User {{ isset($user_data) ? 'Update' : 'Add' }} Form</div>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-body">

                    @if(isset($user_data))
                        {{ Form::open(['url'=>route('user.update', $user_data->id),'class'=>'form','files'=>true]) }}
                        @method('put')
                    @else
                        {{ Form::open(['url'=>route('user.store'),'class'=>'form','files'=>true]) }}
                    @endif

                        <div class="form-group row">
                            {{ Form::label('name','Name',['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::text('name',@$user_data->name,['class'=>'form-control form-control-sm','required'=>true,'id'=>'name','placeholder'=>'Enter name']) }}
                                @error('name')
                                <span class="span-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        @if(!isset($user_data))
                            <div class="form-group row">
                                {{ Form::label('email','Email',['class'=>'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::email('email',@$user_data->email,['class'=>'form-control form-control-sm','required'=>true,'id'=>'email','placeholder'=>'Enter email']) }}
                                    @error('email')
                                    <span class="span-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                {{ Form::label('password','Password',['class'=>'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::password('password',['class'=>'form-control form-control-sm','required'=>true,'id'=>'password','placeholder'=>'Enter password']) }}
                                    @error('password')
                                    <span class="span-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                {{ Form::label('password_confirmation','Re-Password',['class'=>'col-sm-3']) }}
                                <div class="col-sm-9">
                                    {{ Form::password('password_confirmation',['class'=>'form-control form-control-sm','required'=>true,'id'=>'password_confirmation','placeholder'=>'Enter password_confirmation']) }}
                                </div>
                            </div>

                        @endif

                            <div class="form-group row">
                        {{ Form::label('role','Role',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::select('role',['seller'=>'Seller','user'=>'User'],@$user_data->role,['class'=>'form-control form-control-sm','required'=>true]) }}
                            @error('role')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                        <div class="form-group row d-none" >
                            {{ Form::label('status','Status',['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::select('status',['active'=>'Active','inactive'=>'Inactive'],@$user_data->status,['class'=>'form-control form-control-sm','required'=>false]) }}
                                @error('status')
                                <span class="span-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>


                    <div class="form-group row">
                        <div class="col-sm-9 offset-sm-3">
                            {{ Form::button('<i class="fa fa-trash"></i> Re-set',['class'=>'btn btn-danger btn-sm','id'=>'reset','type'=>'reset']) }}
                            {{ Form::button('<i class="fa fa-paper-plane"></i> Submit',['class'=>'btn btn-success btn-sm','id'=>'submit','type'=>'submit']) }}
                        </div>
                    </div>



                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection

