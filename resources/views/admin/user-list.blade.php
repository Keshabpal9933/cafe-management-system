@extends('layouts.admin')

@section('scripts')
    <script>
        $('.delete_btn').click(function (e) {
            var id= $(this).data('id');
            var confirmed = confirm('Are you sure you want to delete the user data ?');
            if(confirmed){
                $('#'+id).submit();
            }
        });
    </script>

@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">User List</div>
                    <div class="ibox-tools">
                        <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                        <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item">option 1</a>
                            <a class="dropdown-item">option 2</a>
                        </div>
                    </div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @if($user_data)

                            @foreach($user_data as $user_info)

                                <tr>
                                    <td>{{ $user_info->name }}</td>
                                    <td>{{ $user_info->email }}</td>
                                    <td>{{ $user_info->role }}</td>

                                    <td>
                                            <span class="badge badge-{{ $user_info->status == 'active' ? 'success' : 'danger' }}">
                                                {{ ucfirst($user_info->status) }}
                                            </span>
                                    </td>

                                    <td>
                                        <a href="{{ route('user.edit', $user_info->id) }}" class="btn btn-success btn-sm">
                                            <i class="fa fa-edit"></i>
                                        </a>

                                        <a href="{{ route('user-change-pwd',$user_info->id) }}" class="btn btn-primary btn-sm " >
                                            <i class="fa fa-key"></i>
                                        </a>


                                        <a href="javascript:;" class="btn btn-danger btn-sm delete_btn" data-id="user-{{ $user_info->id }}">
                                            <i class="fa fa-trash"></i>
                                        </a>

                                        {{ Form::open(['url'=>route('user.destroy', $user_info->id), 'id'=>'user-'.$user_info->id]) }}
                                        @method('delete')
                                        {{ Form::close() }}



                                    </td>

                                </tr>

                            @endforeach

                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
