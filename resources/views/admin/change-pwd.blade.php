@extends('layouts.admin')

@section('scripts')

@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">User {{ isset($user_data) ? 'Update' : 'Add' }} Form</div>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-body">

                        {{ Form::open(['url'=>route('password-change', $user_data->id),'class'=>'form','files'=>true]) }}
                        @method('put')

                        <div class="form-group row">
                            {{ Form::label('password','Password',['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::password('password',['class'=>'form-control form-control-sm','required'=>true,'id'=>'password','placeholder'=>'Enter password']) }}
                                @error('password')
                                <span class="span-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('password_confirmation','Re-Password',['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::password('password_confirmation',['class'=>'form-control form-control-sm','required'=>true,'id'=>'password_confirmation','placeholder'=>'Enter password_confirmation']) }}
                            </div>
                        </div>


                    <div class="form-group row">
                        <div class="col-sm-9 offset-sm-3">
                            {{ Form::button('<i class="fa fa-trash"></i> Re-set',['class'=>'btn btn-danger btn-sm','id'=>'reset','type'=>'reset']) }}
                            {{ Form::button('<i class="fa fa-paper-plane"></i> Submit',['class'=>'btn btn-success btn-sm','id'=>'submit','type'=>'submit']) }}
                        </div>
                    </div>



                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection

