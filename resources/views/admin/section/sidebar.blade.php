<!-- START SIDEBAR-->
<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <div class="admin-block d-flex">
            <div style="background-color: #ccc; padding: 7px; width: 40px; text-align: center; vertical-align: center; border-radius: 50%;">
                <i class="fa fa-user fa-2x"></i>
            </div>
            <div class="admin-info">
                <div class="font-strong">{{{ auth()->user()->name }}}</div><small>{{ auth()->user()->role }}</small></div>
        </div>
        <ul class="side-menu metismenu">
            <li>
                <a class="active" href="{{ route(auth()->user()->role) }}"><i class="sidebar-item-icon fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>

            <li class="heading">FEATURES</li>

            <li>
                <a href=""><i class="sidebar-item-icon fa fa-images"></i>
                    <span class="nav-label">Slider Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('slider.create')}}">Add Slider</a>
                    </li>
                    <li>
                        <a href="{{ route('slider.index') }}">Slider List</a>
                    </li>
                </ul>
            </li>


            <li>
                <a href=""><i class="sidebar-item-icon fa fa-sitemap"></i>
                    <span class="nav-label">Category Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('category.create') }}">Menu Add</a>
                    </li>
                    <li>
                        <a href="{{ route('category.index') }}">Menu List</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href=""><i class="sidebar-item-icon fa fa-shopping-bag"></i>
                    <span class="nav-label">Products Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('product.create') }}">Item Add</a>
                    </li>
                    <li>
                        <a href="{{ route('product.index') }}">Item List </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href=""><i class="sidebar-item-icon fa fa-users"></i>
                    <span class="nav-label">Users Manager</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{ route('user.create') }}">User Add</a>
                    </li>
                    <li>
                        <a href="{{ route('user.index') }}">User List</a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
</nav>
<!-- END SIDEBAR-->
