<header class="header">
    <div class="page-brand">
        <a class="link" href="{{ route(auth()->user()->role) }}">
                    <span class="brand">
                        Admin CMS
                    </span>
            <span class="brand-mini">
                <i class="fa fa-home"></i>
            </span>
        </a>

    </div>
    <div class="flexbox flex-1">
        <!-- START TOP-LEFT TOOLBAR-->
        <ul class="nav navbar-toolbar">
            <li>
                <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
            </li>

        </ul>
        <!-- END TOP-LEFT TOOLBAR-->
        <!-- START TOP-RIGHT TOOLBAR-->
        <ul class="nav navbar-toolbar">

            <li class="dropdown dropdown-user">
                <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                    <i class="fa fa-user"></i> &nbsp;
                    {{ auth()->user()->name }}
                    <i class="fa fa-angle-down m-l-5"></i></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="profile.html"><i class="fa fa-user"></i>Profile</a>
                    <a class="dropdown-item" href="{{ route('user-change-pwd',auth()->user()->id) }}"><i class="fa fa-key"></i>Change Password</a>
                    <li class="dropdown-divider"></li>

                    <a class="dropdown-item" href="javascript:;" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off"></i>Logout
                    </a>

                    {{ Form::open(['url'=>route('logout'), 'id'=>'logout-form']) }}
                    {{ Form::close() }}

                </ul>
            </li>
        </ul>
        <!-- END TOP-RIGHT TOOLBAR-->
    </div>
</header>
