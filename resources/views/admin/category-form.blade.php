@extends('layouts.admin')

@section('scripts')

    <script>
        $('#is_parent').change(function () {
            let is_checked = $(this).prop('checked');
            if(is_checked){
                $('#parent_id').val("");
                $('#parent_div').addClass('d-none');
            }else{
                $('#parent_div').removeClass('d-none');
            }
        });
    </script>

@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Category {{ isset($category_data) ? 'Update' : 'Add' }} Form</div>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-body">

                    @if(isset($category_data))
                        {{ Form::open(['url'=>route('category.update', $category_data->id),'class'=>'form','files'=>true]) }}
                        @method('put')
                    @else
                        {{ Form::open(['url'=>route('category.store'),'class'=>'form','files'=>true]) }}
                    @endif

                    <div class="form-group row">
                        {{ Form::label('title','Title',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::text('title',@$category_data->title,['class'=>'form-control form-control-sm','required'=>true,'id'=>'title','placeholder'=>'Enter title']) }}
                            @error('title')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                        <div class="form-group row">
                            {{ Form::label('is_parent','Is Parent ?',['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::checkbox('is_parent','1',isset($category_data, $category_data->parent_id) && $category_data->parent_id != null ? false : true,['id'=>'is_parent']) }} Yes
                            </div>
                        </div>


                        <div class="form-group row {{ isset($category_data, $category_data->parent_id) && $category_data->parent_id != null ? '' : 'd-none' }}" id="parent_div">
                            {{ Form::label('parent_id','Parent Category',['class'=>'col-sm-3']) }}
                            <div class="col-sm-9">
                                {{ Form::select('parent_id',@$parent_data,@$category_data->parent_id,['class'=>'form-control form-control-sm','placeholder'=>'---Select any one---']) }}
                                @error('parent_id')
                                <span class="span-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>


                    <div class="form-group row">
                        {{ Form::label('status','Status',['class'=>'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::select('status',['active'=>'Active','inactive'=>'Inactive'],@$category_data->status,['class'=>'form-control form-control-sm','required'=>true]) }}
                            @error('status')
                            <span class="span-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>


                        <div class="form-group row">
                            {{ Form::label('image','Image',['class'=>'col-sm-3']) }}
                            <div class="col-sm-4">
                                {{ Form::file('image',['id'=>'image','accept'=>'image/*']) }}
                                @error('image')
                                <span class="span-danger">{{ $message }}</span>
                                @enderror
                            </div>

                        <div class="col-sm-4">
                            @if(isset($category_data))

                                <img src="{{ imageUrl($category_data->image, 'category') }}" alt="" style="max-width: 100px;" class="img img-fluid img-thumbnail">

                            @endif
                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-sm-9 offset-sm-3">
                            {{ Form::button('<i class="fa fa-trash"></i> Re-set',['class'=>'btn btn-danger btn-sm','id'=>'reset','type'=>'reset']) }}
                            {{ Form::button('<i class="fa fa-paper-plane"></i> Submit',['class'=>'btn btn-success btn-sm','id'=>'submit','type'=>'submit']) }}
                        </div>
                    </div>



                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection

