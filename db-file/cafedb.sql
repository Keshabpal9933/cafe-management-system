-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2020 at 05:13 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cafedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cart_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `seller_id` bigint(20) UNSIGNED DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `actual_price` double(8,2) NOT NULL,
  `total_amount` double(8,2) NOT NULL,
  `status` enum('new','cancelled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `cart_code`, `product_id`, `user_id`, `seller_id`, `quantity`, `actual_price`, `total_amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 'cvws3hC0fdeAi87', 23, NULL, NULL, 2, 80.00, 160.00, 'new', '2020-01-31 03:24:11', '2020-01-31 03:24:11'),
(2, 'DPOz1n5bZvJELj8', 19, NULL, NULL, 1, 150.00, 150.00, 'new', '2020-01-31 03:38:50', '2020-01-31 03:38:50'),
(3, 'cylxyczMiLTwiQo', 23, NULL, NULL, 1, 80.00, 80.00, 'new', '2020-01-31 06:00:08', '2020-01-31 06:00:08'),
(4, 'O4UHuWQnNgfJDio', 19, NULL, NULL, 1, 150.00, 150.00, 'new', '2020-01-31 11:13:16', '2020-01-31 11:13:16'),
(5, 'RTeDop0EQPakLmG', 19, NULL, NULL, 1, 150.00, 150.00, 'new', '2020-01-31 21:30:07', '2020-01-31 21:30:07');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `added_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `slug`, `parent_id`, `image`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(34, 'Android', 'android', 34, 'Category-20200106063448630.jpg', 'active', NULL, '2020-01-06 00:49:50', '2020-01-06 03:36:27'),
(35, 'Huwai Honor 8x', 'huwai-honor-8x', 34, NULL, 'active', NULL, '2020-01-06 00:50:04', '2020-01-06 00:50:04'),
(55, 'Break-Fast', 'break-fast', NULL, 'Category-2020013107403336.jpg', 'active', NULL, '2020-01-31 01:55:33', '2020-01-31 01:55:33'),
(56, 'Tea & Coffee', 'tea-coffee', 55, 'Category-20200131074109753.jpg', 'active', NULL, '2020-01-31 01:56:09', '2020-01-31 01:56:09'),
(57, 'Lunch Food', 'lunch-food', NULL, 'Category-20200131074152289.jpg', 'active', NULL, '2020-01-31 01:56:52', '2020-01-31 01:56:52'),
(58, 'Fast-Food', 'fast-food', 57, 'Category-20200131074223325.jpg', 'active', NULL, '2020-01-31 01:57:23', '2020-01-31 01:57:23'),
(59, 'Dinner Food', 'dinner-food', NULL, 'Category-20200131074252128.jpg', 'active', NULL, '2020-01-31 01:57:52', '2020-01-31 01:57:52'),
(60, 'Drinks', 'drinks', NULL, 'Category-20200131074555821.jpg', 'active', NULL, '2020-01-31 02:00:55', '2020-01-31 02:00:55'),
(61, 'Beer', 'beer', 60, 'Category-20200131080128439.jpg', 'active', NULL, '2020-01-31 02:16:28', '2020-01-31 02:16:28'),
(62, 'Pepsi', 'pepsi', 60, 'Category-20200131080302283.jpg', 'active', NULL, '2020-01-31 02:18:02', '2020-01-31 02:18:02');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `message`, `created_at`, `updated_at`) VALUES
(1, 'hello sir, your delivery is completed', '2020-02-01 11:31:01', '2020-02-01 11:31:01'),
(2, 'hello siiii', '2020-02-01 11:50:49', '2020-02-01 11:50:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_26_123028_create_sliders_table', 2),
(5, '2019_12_31_153142_create_categories_table', 3),
(8, '2020_01_06_094350_create_products_table', 4),
(9, '2020_01_06_094602_create_product_images_table', 4),
(10, '2020_01_09_105159_create_pages_table', 5),
(11, '2020_01_13_150400_create_product_reviews_table', 6),
(12, '2020_01_18_165149_create_carts_table', 7),
(13, '2020_01_18_165236_create_orders_table', 7),
(14, '2020_02_01_155216_create_messages_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cart_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `total_amount` double(8,2) NOT NULL,
  `status` enum('new','verified','processed','delivered','cancelled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `cart_code`, `user_id`, `total_amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 'cvws3hC0fdeAi87', NULL, 160.00, 'new', '2020-01-31 03:24:11', '2020-01-31 03:24:11'),
(2, 'DPOz1n5bZvJELj8', NULL, 150.00, 'new', '2020-01-31 03:38:51', '2020-01-31 03:38:51'),
(3, 'cylxyczMiLTwiQo', NULL, 80.00, 'new', '2020-01-31 06:00:08', '2020-01-31 06:00:08'),
(4, 'O4UHuWQnNgfJDio', NULL, 150.00, 'new', '2020-01-31 11:13:16', '2020-01-31 11:13:16'),
(5, 'RTeDop0EQPakLmG', NULL, 150.00, 'new', '2020-01-31 21:30:07', '2020-01-31 21:30:07');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `summary`, `description`, `image`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'About Us', 'about-us', 'This is about us page summary', '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 1rem; line-height: 1.4em; font-family: Lato; vertical-align: baseline; text-align: justify; color: rgb(51, 51, 51);\">Broadway Infosys Nepal is one of the best inclusive computer training institutes in Kathmandu, Nepal. Established in 2008, our professional IT Training and Development center has been employing experts in this field to impart professional education to trainees. We offer well-structured complete professional training in various Programming Languages, Graphics &amp; Multimedia, Web Designing as well as Development Training that is based upon the current recruitment needs in the IT market.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 1rem; line-height: 1.4em; font-family: Lato; vertical-align: baseline; text-align: justify; color: rgb(51, 51, 51);\">To summarize, Broadway Infosys is a complete learning institute that not only provides training on various IT courses but also prepares students to smartly handle the real working environment. We are dedicated and committed towards:</p><ul><li style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 1rem; line-height: 1.4em; font-family: Lato; vertical-align: baseline; text-align: justify; color: rgb(51, 51, 51);\"><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: left;\">Providing quality IT training to the aspiring IT professionals</span></li><li style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 1rem; line-height: 1.4em; font-family: Lato; vertical-align: baseline; text-align: justify; color: rgb(51, 51, 51);\"><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: left;\">Availability of highly qualified and experienced instructors</span></li><li style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 1rem; line-height: 1.4em; font-family: Lato; vertical-align: baseline; text-align: justify; color: rgb(51, 51, 51);\"><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: left;\">Assigning project works as&nbsp;</span></li><li style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 1rem; line-height: 1.4em; font-family: Lato; vertical-align: baseline; text-align: justify; color: rgb(51, 51, 51);\"><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: left;\">Assigning project works as Assigning project works as&nbsp;</span></li><li style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 15px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 1rem; line-height: 1.4em; font-family: Lato; vertical-align: baseline; text-align: justify; color: rgb(51, 51, 51);\"><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, sans-serif; text-align: left;\">Assigning project works as Assigning project works as Assigning project works as Assigning project works as&nbsp;<br></span><br></li></ul><p style=\"margin: 0px; padding: 2px 0px 2px 20px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 1rem; line-height: inherit; font-family: inherit; vertical-align: baseline; position: relative; display: block; list-style-type: none;\"><br></p><p style=\"margin: 0px; padding: 2px 0px 2px 20px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 1rem; line-height: inherit; font-family: inherit; vertical-align: baseline; position: relative; display: block; list-style-type: none;\"><br></p><p style=\"margin: 0px; padding: 2px 0px 2px 20px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 1rem; line-height: inherit; font-family: inherit; vertical-align: baseline; position: relative; display: block; list-style-type: none;\"><br></p>', 'Pages-20200109020231454.jpg', NULL, NULL, '2020-01-13 04:18:12'),
(2, 'Privacy policy', 'privacy-policy', 'This is Privacy policy page ', '<p>Privacy Police Description</p>', 'Pages-20200109020310688.jpg', NULL, NULL, '2020-01-09 08:18:12'),
(3, 'Term & Condition', 'term-condition', 'This is Term & Condition page ', NULL, NULL, NULL, NULL, NULL),
(4, 'Return policy', 'return-policy', 'This is Return policy page ', NULL, NULL, NULL, NULL, '2020-01-10 22:45:42'),
(37, 'Contact us', 'contact-us', 'This is Contact us page ', NULL, NULL, NULL, NULL, NULL),
(38, 'Help & FAQ', 'help-faq', 'This is Help & FAQ page ', NULL, 'Pages-20200111050655794.jpg', NULL, NULL, '2020-01-10 23:21:56');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_id` bigint(20) UNSIGNED DEFAULT NULL,
  `sub_cat_id` bigint(20) UNSIGNED DEFAULT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `delivery_charge` double(8,2) DEFAULT NULL,
  `actual_cost` double(8,2) NOT NULL,
  `image` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `seller_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `added_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `slug`, `cat_id`, `sub_cat_id`, `summary`, `description`, `price`, `discount`, `delivery_charge`, `actual_cost`, `image`, `is_featured`, `seller_id`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(19, 'Chicken Rice', 'chicken-rice', 57, 58, 'chicken rice make from chicken and rice', '<p>chicken rice make from chicken and rice<br></p>', 150.00, 0.00, 0.00, 150.00, 'Product-20200131080812690.jpg', 1, NULL, 'active', NULL, '2020-01-31 02:23:12', '2020-01-31 02:23:12'),
(20, 'Chicken Chaw Min', 'chicken-chaw-min', 57, 58, 'chicken chaw min make chicken and chaumin', '<p>chicken chaw min make chicken and chaumin<br></p>', 150.00, 0.00, 0.00, 150.00, 'Product-20200131081022690.jpg', 1, NULL, 'active', NULL, '2020-01-31 02:25:22', '2020-01-31 02:25:22'),
(21, 'Milk Tea', 'milk-tea', 55, 56, 'milk tea make different types of spaces and milk', '<p>milk tea make different types of spaces and milk<br></p>', 35.00, 0.00, 0.00, 35.00, 'Product-20200131081200747.jpg', 1, NULL, 'active', NULL, '2020-01-31 02:27:01', '2020-01-31 02:27:01'),
(22, 'Black Tea', 'black-tea', 55, 56, 'black tea make simply easy', '<p>black tea make simply easy&nbsp;<br></p>', 20.00, 0.00, 0.00, 20.00, 'Product-2020013108130723.jpg', 1, NULL, 'active', NULL, '2020-01-31 02:28:07', '2020-01-31 02:28:07'),
(23, 'Chaumin Veg', 'chaumin-veg', 57, 58, 'chaumin veg simply make different types of  vegetables', '<p>chaumin veg simply make different types of&nbsp; vegetables<br></p>', 80.00, 0.00, 0.00, 80.00, 'Product-20200131081538403.jpg', 1, NULL, 'active', NULL, '2020-01-31 02:30:38', '2020-01-31 02:30:38'),
(24, 'Thakali Khana', 'thakali-khana', 59, NULL, 'thakali khana is very famous in nepal', '<p>thakali khana is very famous in nepal<br></p>', 280.00, 0.00, 0.00, 280.00, 'Product-20200131081759798.jpg', 1, NULL, 'active', NULL, '2020-01-31 02:32:59', '2020-01-31 02:32:59'),
(25, 'Special nepal khaja', 'special-nepal-khaja', 59, NULL, 'special nepali khaja make different disces  for non-veg person', '<p>special nepali khaja make different disces&nbsp; for non-veg person<br></p>', 300.00, 0.00, 0.00, 300.00, 'Product-20200131083854737.jpg', 1, NULL, 'active', NULL, '2020-01-31 02:53:54', '2020-01-31 02:53:54'),
(26, 'Chicken Pizza', 'chicken-pizza', 59, NULL, 'it’s loaded with protein-rich chicken and black beans, it’s hearty enough to satisfy everyone!', '<p><span style=\"color: rgb(60, 64, 67); font-family: arial, sans-serif;\">it’s loaded with protein-rich&nbsp;</span><span style=\"font-weight: bold; color: rgb(82, 86, 90); font-family: arial, sans-serif;\">chicken</span><span style=\"color: rgb(60, 64, 67); font-family: arial, sans-serif;\">&nbsp;and black beans, it’s hearty enough to satisfy everyone!</span><wbr style=\"color: rgb(60, 64, 67); font-family: arial, sans-serif;\"><span style=\"color: rgb(60, 64, 67); font-family: arial, sans-serif;\">&nbsp;</span><br></p>', 350.00, 0.00, 0.00, 350.00, 'Product-20200201043124801.jpg', 1, NULL, 'active', NULL, '2020-02-01 10:46:24', '2020-02-01 10:46:24'),
(27, 'Checken Loli-Pop', 'checken-loli-pop', 57, 58, 'Chicken lollipop is an hors d\'oeuvre popular in Indian Chinese cuisine.', '<p><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif;\">Chicken lollipop is an hors d\'oeuvre popular in Indian Chinese cuisine.</span><br></p>', 260.00, 0.00, 0.00, 260.00, 'Product-20200201051116998.jpg', 1, NULL, 'active', NULL, '2020-02-01 11:26:17', '2020-02-01 11:26:17'),
(28, 'Black Coffee', 'black-coffee', 55, 56, 'Black coffee is the powerhouse of antioxidants. Black coffee contains Vitamin B2, B3, B5, Manganese, potassium and magnesium.', '<p><b style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 16px;\">Black coffee</b><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 16px;\">&nbsp;is the powerhouse of antioxidants.&nbsp;</span><b style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 16px;\">Black coffee</b><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 16px;\">&nbsp;contains Vitamin B2, B3, B5, Manganese, potassium and magnesium.</span><br></p>', 30.00, 0.00, 0.00, 30.00, 'Product-20200201051357549.jpg', 1, NULL, 'active', NULL, '2020-02-01 11:28:57', '2020-02-01 11:28:57');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image_name`, `created_at`, `updated_at`) VALUES
(64, 19, 'Product-202001310808126.jpg', '2020-01-31 02:23:12', '2020-01-31 02:23:12'),
(65, 20, 'Product-20200131081022713.jpg', '2020-01-31 02:25:22', '2020-01-31 02:25:22'),
(66, 21, 'Product-20200131081201444.jpg', '2020-01-31 02:27:01', '2020-01-31 02:27:01'),
(67, 22, 'Product-20200131081307823.jpg', '2020-01-31 02:28:07', '2020-01-31 02:28:07'),
(68, 23, 'Product-20200131081538329.jpg', '2020-01-31 02:30:38', '2020-01-31 02:30:38'),
(69, 24, 'Product-20200131081759977.jpg', '2020-01-31 02:32:59', '2020-01-31 02:32:59'),
(70, 25, 'Product-20200131083854176.jpg', '2020-01-31 02:53:54', '2020-01-31 02:53:54'),
(71, 26, 'Product-20200201043124397.jpg', '2020-02-01 10:46:25', '2020-02-01 10:46:25'),
(72, 27, 'Product-20200201051117434.jpg', '2020-02-01 11:26:17', '2020-02-01 11:26:17'),
(73, 28, 'Product-2020020105135746.jpg', '2020-02-01 11:28:57', '2020-02-01 11:28:57');

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `rating` tinyint(4) NOT NULL DEFAULT 0,
  `review` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `added_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `image`, `link`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(13, 'ChickenSandwitch', 'Slider-20200131073649865.jpg', NULL, 'active', NULL, '2020-01-31 01:51:49', '2020-01-31 01:51:49'),
(14, 'Ice-Cream', 'Slider-20200131073705892.jpg', NULL, 'active', NULL, '2020-01-31 01:52:05', '2020-01-31 01:52:05'),
(15, 'Fry-Fish', 'Slider-20200131073732964.jpg', NULL, 'active', NULL, '2020-01-31 01:52:32', '2020-01-31 01:52:32'),
(16, 'chicken masala', 'Slider-20200131073900453.jpg', NULL, 'active', NULL, '2020-01-31 01:54:00', '2020-01-31 01:54:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('admin','seller','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(16, 'james pal', 'admin@gmail.com', NULL, '$2y$10$NmQZz/mVMXRz25Nvi4ujuurgQPj4ge2N4NA98DIWnzSMQwd.xh8hK', 'admin', 'active', NULL, '2020-02-01 22:24:45', '2020-02-01 22:24:45'),
(17, 'Admin User', 'admin@cafe.com', NULL, '$2y$10$t3BSCHPRQBXKDHBY/E3rh.dvbdhS6mGP7Fb9tSDuq6ExAdfup9XF6', 'admin', 'active', NULL, NULL, NULL),
(18, 'Customer One', 'user@cafe.com', NULL, '$2y$10$fORwrNc4o4NH1fyl03IK.uy541JPvWde9k0qYGsvoBXNAG6JH/8hq', 'user', 'active', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_product_id_foreign` (`product_id`),
  ADD KEY `carts_user_id_foreign` (`user_id`),
  ADD KEY `carts_seller_id_foreign` (`seller_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`),
  ADD KEY `categories_added_by_foreign` (`added_by`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orders_cart_code_unique` (`cart_code`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`),
  ADD KEY `pages_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`),
  ADD KEY `products_cat_id_foreign` (`cat_id`),
  ADD KEY `products_sub_cat_id_foreign` (`sub_cat_id`),
  ADD KEY `products_seller_id_foreign` (`seller_id`),
  ADD KEY `products_added_by_foreign` (`added_by`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_reviews_product_id_foreign` (`product_id`),
  ADD KEY `product_reviews_user_id_foreign` (`user_id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sliders_added_by_foreign` (`added_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `carts_seller_id_foreign` FOREIGN KEY (`seller_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_added_by_foreign` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_added_by_foreign` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `products_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `products_seller_id_foreign` FOREIGN KEY (`seller_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `products_sub_cat_id_foreign` FOREIGN KEY (`sub_cat_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `sliders`
--
ALTER TABLE `sliders`
  ADD CONSTRAINT `sliders_added_by_foreign` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
