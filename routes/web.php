<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/****************** Route for front-end **********************/
Route::get('/products','ProductController@getAllProducts')->name('product-list');       //defined in cafe menu
Route::get('/products/featured','ProductController@getAllFeaturedProducts')->name('featured-products');
Route::get('/product/{slug}','ProductController@show')->name('product-detail');    //define in product-list   product's title
Route::get('/category/{slug}','CategoryController@getAllProductsByCategory')->name('category-products');    //defined in Helpers.php
Route::get('/category/{parent_slug}/{slug}','CategoryController@getAllProductsByChildCategory')->name('child-category-products');
Route::post('/product/{slug}','ProductController@submitReview')->name('product-review');
Route::get('/cart','CartController@showCart')->name('cart-list');
Route::get('/checkout','CartController@checkout')->name('checkout')->middleware(['auth','user']);
//route for cart
Route::post('/cart/add','CartController@addToCart')->name('add-to-cart');
Route::get('/loadmore', 'LoadMoreController@index');
Route::post('/loadmore/load_data', 'LoadMoreController@load_data')->name('loadmore.load_data');
Route::get('/esewa','CartController@esewa')->name('esewa');


/****************** Route for front-end **********************/



/****************** Route for user redirect on their dashboard **********************/

Route::group(['prefix'=>'admin','middleware'=>['auth','admin']],function (){
    Route::get('/','HomeController@admin')->name('admin');
    Route::resource('slider','SliderController');
    Route::resource('category','CategoryController');
    Route::resource('product','ProductController');
    Route::resource('message','MessageController');
    Route::resource('user','UserController');
    Route::get('/change-pwd/{id}','UserController@changePassword')->name('user-change-pwd');
    Route::put('/change-pwd/{id}','UserController@submitChangePassword')->name('password-change');
});

//for show sub-category in the product-form
Route::post('/get-child','CategoryController@getChildByParent')->name('get-child')->middleware('auth');
//


Route::group(['prefix'=>'seller','middleware'=>['auth','seller']],function (){
    Route::get('/','HomeController@seller')->name('seller');

    Route::resource('products','SellerProductController');
});

Route::group(['prefix'=>'user','middleware'=>['auth','user']],function (){
    Route::get('/','HomeController@user')->name('user');
});

/****************** Route for user redirect on their dashboard **********************/






//Auth route for login and register

Route::get('/', 'FrontendController@index')->name('homepage');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Auth route for login and register
