<?php

// For fetch dynamic category and sun-category in the front-end
function getHeaderMenu(){
    $category = new \App\Models\Category();
    $category = $category->getCategoryMenu();
    if($category){
        echo '<li>';
        echo '<a href="" class="dropbtn">Category</a>';
        echo '<ul class="dropdown">';
            foreach ($category as $category_data){
                if($category_data->childrens->count()){
                    echo '<li style="width: 250px" ><a href="">'.$category_data->title.'</a>';
                    echo '<ul class="dropdown-subcontent">';

                        foreach ($category_data->childrens as $child_cat){
                            echo '<li><a href="'.route('child-category-products',[$category_data->slug, $child_cat->slug]).'">'.$child_cat->title.'</a></li>';
                        }

                    echo '</ul>';
                    echo '</li>';
                }else{
                    echo '<li style="width: 250px"><a href="'.route('category-products', $category_data->slug).'">'.$category_data->title.'</a></li>';
                }
            }

        echo '</ul>';
        echo '</li>';
    }


}



// For Delete Image
function deleteImage($image_name, $dir){
    if(file_exists(public_path().'/uploads/'.$dir.'/'.$image_name)){
        unlink(public_path().'/uploads/'.$dir.'/'.$image_name);
    }

    if(file_exists(public_path().'/uploads/'.$dir.'/Thumb-'.$image_name)){
        unlink(public_path().'/uploads/'.$dir.'/Thumb-'.$image_name);
    }
}


//for show image in the listing page
function imageUrl($image_name, $dir){
    if ($image_name != null){
        $path = public_path()."/uploads/".$dir."/";
        $url = asset('uploads/'.$dir."/");

        if(file_exists($path."Thumb-".$image_name)){
            return asset($url.'/Thumb-'.$image_name);
        }elseif (file_exists($path.'/'.$image_name)){
            return asset($url.'/'.$image_name);
        }else{
            return asset('images/default.png');
        }

    }else{
        return asset('images/default.png');
    }
}





//For Image Upload
function uploadImage($file,$dir,$thumb=false){
    if($file){
        $path = public_path().'/uploads/'.$dir;      // defin a path ==>  public/uploads   $dir = define from slidercontroller
        if(File::exists($path)){
            File::makeDirectory($path,0777, true, true);   // make directory
        }
        $file_name = ucfirst($dir)."-".date('Ymdhis').rand(0,999).".".$file->getClientOriginalExtension();
        $success = $file->move($path, $file_name);   //file has been uploaded and save in database
        if($success){
            if($thumb){
                list($width, $height) = explode("x",$thumb);
                Image::make($path."/".$file_name)->resize($width,$height, function ($constraint){
                    return $constraint->aspectRatio();
                })->save($path."/Thumb-".$file_name);    // if you want save 2 image original and thumbnail image than wirte "/Thumb-" else dont write it will save thumbnail image only
            }

            return $file_name;

        }else{
            return false;
        }
    }else{
        return false;
    }
}
