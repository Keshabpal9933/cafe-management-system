<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['title','slug','image','parent_id','status','added_by'];


    //fatch sub category   -- parent to child
    function child_cats(){
        return $this->hasMany('App\Models\Category','parent_id','id');
    }

    function childrens(){
        return $this->hasMany('App\Models\Category','parent_id','id')->where('status','active');
    }


    // make slug using title
    function getSlug($title){
        $slug = \Str::slug($title);
        if($this->where('slug',$slug)->count() > 0){         // if category already exist than apend date and rand else no neet to apend data and rand
            $slug .= date('Ymdhis').rand(0,999);
        }

        return $slug;
    }


    //to fetch the All Category
    public function getCategoryMenu(){                  // this function is defined in Helpers.php
        return $this->where('parent_id',null)->where('status','active')->with('childrens')->get();      //childrens is defined above
    }

        //validation
    function getRules(){
        $array = array(
            'title'=>'required|string',
            'image'=>'sometimes|image|max:5000',
            'parent_id'=>'nullable|exists:categories,id',
            'status'=>'required|in:active,inactive'
        );
        return $array;
    }



}
