<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title','summary','description','slug','cat_id','sub_cat_id','actual_cost','price','discount','delivery_charge','image','is_featured','seller_id','status','added_by'];

    //
    public function cat_info(){
        return $this->hasOne('\App\Models\Category','id','cat_id');
    }

    public function sub_cat_info(){
        return $this->hasOne('\App\Models\Category','id','sub_cat_id');
    }

    public function seller_info(){
        return $this->hasOne('\App\User','id','seller_id');
    }


    //for product_image
    public  function images(){
        return $this->hasMany('App\Models\ProductImage','product_id','id');
    }

    //to show related image in product-detail page    // call from ProductController@show
    public function related_products(){
        return $this->hasMany('App\Models\Product','cat_id','cat_id')->where('status','active')->limit(12);
    }


    //to show the review data
    public function reviews(){
        return $this->hasMany("App\Models\ProductReview",'product_id','id')->where('status','active')->with('user_info');    // this user_info is defined in Models/ProductReview.php
    }


    // make slug
    public function getSlug($title){
        $slug = \Str::slug($title);
        if($this->where('slug',$slug)->count() > 0){
            $slug .= time().rand(0,999);
        }
        return $slug;
    }


    public function getRules($act='add'){
        $array = array(
            'title'=>'required|string',
            'summary'=>'required|string',
            'description'=>'nullable|string',
            'cat_id'=>'required|exists:categories,id',
            'sub_cat_id'=>'nullable|exists:categories,id',
            'price'=>'required|numeric|min:10',
            'discount'=>'nullable|numeric|min:0|max:100',
            'delivery_charge'=>'nullable|numeric|min:0',
            'image'=>'required|image|max:5000',
            'is_featured'=>'nullable|in:1',
            'seller_id'=>'nullable|exists:users,id',
            'status'=>'required|in:active,inactive',
            'rel_image.*'=>'sometimes|image|max:5000'
        );

        if($act != 'add'){
            $array['image'] = 'sometimes|image|max:5000';
        }

        return $array;

    }




}


