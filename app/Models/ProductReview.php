<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    protected $fillable = ['product_id','user_id','rating','review','status'];

    //for fetch the user id to show user details in review
    public function user_info(){
        return$this->hasOne('App\User','id','user_id');
    }
}
