<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['cart_code','user_id','total_amount','status'];

    public function user_info(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function product_info(){
        return $this->hasOne('\App\Models\Product','id','product_id');
    }
    public function order_info(){
        return $this->hasOne('\App\Models\Order','cart_code','cart_code');
    }

}


