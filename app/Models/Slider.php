<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['title','link','image','added_by','status'];



    public function getRules($act = 'add'){                     // call in the SliderController@store
        $array = array(                             // $act = 'add'  for change the image validation
            'title'=>'required|string',
            'link'=>'nullable|url',
            'image'=>'required|image|max:5000',
            'status'=>'required|in:active,inactive'
        );

        if($act != 'add'){
            $array['image'] = "sometimes|image|max:5000";
        }

        return $array;
    }





}

