<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use App\User;

class SellerProductController extends Controller
{


    protected $category = null;
    protected $user = null;
    protected $product = null;

    public function __construct(Category $category, User $user, Product $product)
    {
        $this->category = $category;
        $this->user= $user;
        $this->product= $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->product = $this->product
            ->with(['cat_info','sub_cat_info'])      // define in product model
            ->where('seller_id',request()->user()->id)   //fetch currently logged in seller id
            ->get();
        return view('seller.product-list')->with('product_data',$this->product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $all_parents = $this->category->where('parent_id',Null)->orderBy('title','ASC')->pluck('title','id');
        return view('seller.product-form')
            ->with('parent_data',$all_parents);     //parent_data is defined in category column
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add(['seller_id'=>$request->user()->id]);
        $request->request->add(['status'=>'inactive']);    //pass the default status for seller product
        
        $rules = $this->product->getRules();
        $request->validate($rules);

        $data = $request->except(['image','rel_image']);

        $data['added_by'] = $request->user()->id;
        $data['slug'] = $this->product->getSlug($request->title);

           //for actual_cost
        $price = $request->price;
        $price = $price - (($price * $request->discount)/100);
        $price = $price + $request->delivery_charge;
        $data['actual_cost'] = $price;


        if($request->image){
            $image_name = uploadImage($request->image,'product',env('PRODUCT_THUMB_SIZE','200x200'));
            if($image_name){
                $data['image'] = $image_name;
            }
        }

        $this->product->fill($data);
        $success = $this->product->save();
        if($success){

//            for multiple image upload
            if($request->rel_image){
                foreach ($request->rel_image as $related_images){
                    $img_name = uploadImage($related_images,'product',env('PRODUCT_THUMB_SIZE','200x200'));
                    if($img_name){
                        $product_image = new ProductImage();
                        $product_image->fill([
                            'product_id'=>$this->product->id,
                            'image_name'=>$img_name
                        ]);
                        $product_image->save();
                    }
                }
            }

            request()->session()->flash('success','Product Added Successfully.');
        }else{
            request()->session()->flash('error','Sorry! There was proble while adding product');
        }

        return redirect()->route('products.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->product = $this->product->with('images')->where('seller_id',request()->user()->id)->find($id);         //images is defined in Models/Product.php
        if(!$this->product){
            request()->session()->flash('error','Sorry! There is problem while deleting the product.');
            return  redirect()->route('products.index');
        }


        $all_parents = $this->category->where('parent_id',Null)->orderBy('title','ASC')->pluck('title','id');
        return view('seller.product-form')
            ->with('product_data',$this->product)
            ->with('parent_data',$all_parents);     //parent_data is defined in category column


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->product = $this->product->with('images')->find($id);         //images is defined in Models/Product.php
        if(!$this->product){
            request()->session()->flash('error','Sorry! There is problem while deleting the product.');
            return  redirect()->route('product.index');
        }


        $rules = $this->product->getRules('update');
        unset($rules['seller_id']);    //unset validation for seller_id and status
        unset($rules['status']);
        $request->validate($rules);

        $data = $request->except(['image','rel_image']);

//        //for actual_cost
        $price = $request->price;
        $price = $price - (($price * $request->discount)/100);
        $price = $price + $request->delivery_charge;
        $data['actual_cost'] = $price;


        if($request->image){
            $image_name = uploadImage($request->image,'product',env('PRODUCT_THUMB_SIZE','200x200'));
            if($image_name){
                $data['image'] = $image_name;

                deleteImage($this->product->image,'product');          //delete old image while uploading new image
            }
        }

        $this->product->fill($data);
        $success = $this->product->save();
        if($success){

//            for multiple image upload
            if($request->rel_image){
                foreach ($request->rel_image as $related_images){
                    $img_name = uploadImage($related_images,'product',env('PRODUCT_THUMB_SIZE','200x200'));
                    if($img_name){
                        $product_image = new ProductImage();
                        $product_image->fill([
                            'product_id'=>$this->product->id,
                            'image_name'=>$img_name
                        ]);
                        $product_image->save();
                    }
                }
            }

            //for delete related image
            if(isset($request->del_image) && !empty($request->del_image)){
                foreach ($request->del_image as $del_image){
                    $prod_image = new ProductImage();
                    $prod_image = $prod_image->where('image_name',$del_image)->first();
                    $success = $prod_image->delete();
                    if($success){
                        deleteImage($del_image,'product');
                    }
                }
            }

            request()->session()->flash('success','Product Updated Successfully.');
        }else{
            request()->session()->flash('error','Sorry! There was proble while updating product');
        }

        return redirect()->route('products.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->product = $this->product->with('images')->where('seller_id',request()->user()->id)->find($id);         //images is defined in Models/Product.php
        if(!$this->product){
            request()->session()->flash('error','Sorry! There is problem while deleting the product.');
            return  redirect()->route('products.index');
        }

        $rel_image = $this->product->images;
        $image = $this->product->image;

        $del = $this->product->delete();

        if($del){
            deleteImage($image, 'product');     //delete single image
            foreach ($rel_image as $del_image){
                deleteImage($del_image->image_name, 'product');
            }
            request()->session()->flash('success','Product deleted Successfully.');
        }else{
            request()->session()->flash('error','sorry there is problem while deleting product');
        }

        return redirect()->route('products.index');
    }
}
