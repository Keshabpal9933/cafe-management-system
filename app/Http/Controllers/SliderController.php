<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    // making object of slider
    protected $slider = null;
    public function __construct(Slider $slider)     //use slider with namespase is must be defined above
    {
        $this->slider = $slider;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_sliders = $this->slider->get();
        return view('admin.slider-list')->with('slider_data', $all_sliders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $rules = $this->slider->getRules();
        $request->validate($rules);

//        dd($request->all());

        $data = $request->except('image');
        $data['added_by'] = $request->user()->id;    //for assign added_by column froeign key to fill the data

        // for image start
            if($request->image){
                $image_name = uploadImage($request->image, 'slider', env('SLIDER_IMAGE_SIZE','1500x300'));
                if($image_name){
                    $data['image'] = $image_name;
                }
            }
        // for image end

        $this->slider->fill($data);
        $status = $this->slider->save();
        if($status){
            $request->session()->flash('success','Slider added successfully.');
        }else{
            $request->session()->flash('error','Sorry! There is an problem while adding the slider.');
        }

        return redirect()->route('slider.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->slider = $this->slider->find($id);    //find coming id from form
        if(!$this->slider){
            request()->session()->flash('error','Sorry! Slider does not exists or has been deleted already');
            return  redirect()->route('slider.index');
        }

        return view('admin.slider-form')->with('slider_data', $this->slider);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->slider = $this->slider->find($id);    //find coming id from form
        if(!$this->slider){
            request()->session()->flash('error','Sorry! Slider does not exists or has been deleted already');
            return  redirect()->route('slider.index');
        }


        $rules = $this->slider->getRules('upload');     //pass argument for image validation change, defined in Models/Slider.php getRules functiion ma
        $request->validate($rules);

//        dd($request->all());

        $data = $request->except('image');

        // for image start
        if($request->image){
            $image_name = uploadImage($request->image, 'slider', env('SLIDER_IMAGE_SIZE','1500x300'));
            if($image_name){
                $data['image'] = $image_name;

                if($this->slider->image != null){
                    deleteImage($this->slider->image, 'slider');    //deleteImage(); is defined in Helpers.php
                }
            }
        }
        // for image end

        $this->slider->fill($data);
        $status = $this->slider->save();
        if($status){
            $request->session()->flash('success','Slider Updated successfully.');
        }else{
            $request->session()->flash('error','Sorry! There is an problem while Updating the slider.');
        }

        return redirect()->route('slider.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->slider = $this->slider->find($id);    //find coming id from form
        if(!$this->slider){
            request()->session()->flash('error','Sorry! Slider does not exists or has been deleted already');
            return  redirect()->route('slider.index');
        }

        $image = $this->slider->image;
        $del = $this->slider->delete();

        if($del){
            //delete image
            if($image != null){
                deleteImage($image, 'slider');              //this deleteImage function is defined in Helpers.php
            }

        //delete data
            request()->session()->flash('success','Slider Deleted Successfully . ');
        }else{
            request()->session()->flash('error','Sorry! There was a proble while deleting the slider .');
        }

        return  redirect()->route('slider.index');

    }
}
