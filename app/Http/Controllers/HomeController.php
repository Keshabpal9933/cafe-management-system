<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use Illuminate\Http\Request;
use App\User;
use App\Models\Product;

class HomeController extends Controller
{

    protected $user = null;
    protected $product = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user, Product $product,Order $order, Cart $cart)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->product = $product;
        $this->order = $order;
        $this->cart = $cart;

    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect()->route(request()->user()->role);
    }



    public function admin(){
        $this->cart = $this->cart
            ->with(['user_info'])      // define in product model
            ->get();
            $this->user = $this->user->where('role','!=','admin')->count();
            // 'order_count' => $this->order->where('status','new')->count(),
            // 'total_income' => $this->order->sum('total_amount'),
            $this->order = $this->order->where('status','new')->count();
            $this->product = $this->product->where('status','active')->count();

        return view('admin.dashboard')
            ->with('user',$this->user)
            ->with('product',$this->product)
            ->with('order',$this->order)
            ->with('cart',$this->cart);


    }

    public function seller(){
        return view('seller.dashboard');
    }

    public function user(){
        return view('user.dashboard');
    }

    /*public function show($cart_code){
        $this->cart = $this->cart
            ->where('cart_code',$cart_code)
            ->firstOrFail();

        return view('admin.card-detail')->with('card_data', $this->cart);


    }*/



}
