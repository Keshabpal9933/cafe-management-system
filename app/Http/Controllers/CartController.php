<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;

class CartController extends Controller
{
    protected $product =  null;

    public function __construct(User $user, Product $product,Cart $cart,Order $order)
    {
        $this->product = $product;
        $this->user = $user;
        $this->cart =$cart;
        $this->order = $order;
    }

    public function addtoCart(Request $request){
//        dd($request->all());

        $this->product = $this->product->find($request->prod_id);
        if(!$this->product){
            return response()->json(['status'=>false,'msg'=>'Invalid Product Id','data'=>null]);
        }

        $current_item = array(
            'id'=>$this->product->id,
            'title'=>$this->product->title,
            'link'=>route('product-detail',$this->product->slug),
            'image_link'=> imageUrl($this->product->image,'product'),
            'price' => $this->product->price,
            'actual_price'=>$this->product->actual_cost,
            'seller_id'=>$this->product->seller_id
        );

        $cart = session('cart') ? session('cart') : array();

        $msg = $this->product->title.'has been updated in your cart.';

        if($cart){
            //cart is not empty         -update cart
            $index = null;

            if(!empty($cart)){
                foreach ($cart as $cart_index => $cart_items){
                    if($cart_items['id'] == $this->product->id){
                        $index = $cart_index;
                        break;
                    }
                }

            if($index === null){
                $current_item['quantity'] = $request->qty;
                $current_item['total_amount'] = $this->product->actual_cost * $request->qty;
                $cart[] = $current_item;
            }else{
                $cart[$index]['quantity'] = $request->qty;
                $cart[$index]['total_amount'] = $request->qty * $this->product->actual_cost;

                //for delete product from add to cart
                if($cart[$index]['quantity'] <= 0){
                    unset($cart[$index]);
                    $msg = $this->product->title."has been removed from your cart.";
                }
            }

            }else{
                $current_item['quantity'] = $request->qty;
                $current_item['total_amount'] = $this->product->actual_cost * $request->qty;
                $cart[] = $current_item;
            }


        }else{
            //cart is empty         -add cart
            $current_item['quantity'] = $request->qty;
            $current_item['total_amount'] = $this->product->actual_cost * $request->qty;
            $cart[] = $current_item;
        }

        //now update into the session
        session()->put('cart',$cart);

        return response()->json(['status',true,'data'=>session('cart'),'msg'=>$msg]);

    }

     public function showCart(){
        return view('home.cart');
    }





    public function checkout(){
        $cart = session('cart');
        if ($cart){
            $cart_code = \Str::random(15);
            $total = 0;
            foreach ($cart as $cart_items){
               $temp = array(
                   'cart_code'=>$cart_code,
                   'user_id'=>request()->user()->id,
                   'product_id'=>$cart_items['id'],
                   'seller_id'=>$cart_items['seller_id'],
                   'quantity'=>$cart_items['quantity'],
                   'actual_price'=>$cart_items['actual_price'],
                   'total_amount'=>$cart_items['total_amount'],
                   'status'=>'new'
               );
               $cart_obj = new Cart();
               $cart_obj->fill($temp);
               $cart_obj->save();
               $total += $cart_items['total_amount'];
            }
            $order = new Order();
            $order_data = array(
                'cart_code'=>$cart_code,
                'user_id'=>request()->user()->id,
                'total_amount'=>$total,
                'status'=>'new'
            );
            $order->fill($order_data);
            $order->save();

            session()->forget('cart');

            // you can send bill as through mail so you can create mail section here

            // Mail section ending

            request()->session()->flash('success','Thank you. Your order has been placed successfully');

            return redirect()->route('user');
        }else{
            return redirect()->back();
        }
    }

}
