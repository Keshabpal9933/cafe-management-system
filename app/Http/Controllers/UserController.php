<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $user = null;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->user = $this->user->where('id','!=',auth()->user()->id)->get();
        return view('admin.user-list')->with('user_data',$this->user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password'=>'required|string|min:8|confirmed',
            'role' => 'required|in:user,seller',
            'status' => 'nullable|in:active,inactive'
        );
        $request->validate($rules);
        $data = $request->all();

        $data['password'] = Hash::make($request->password);       //to make incripted password

        $this->user->fill($data);
        $success = $this->user->save();
        if($success){
            //for send mail -- to = $this->user->email
            request()->session()->flash('success','User added successfully');
        }else{
            request()->session()->flash('error','Sorry! There was problem while adding the user');
        }

        return redirect()->route('user.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->user = $this->user->find($id);
        if(!$this->user){
            request()->session()->flash('error','User not found.');
            return redirect()->route('user.index');
        }

        return view('admin.user-form')->with('user_data',$this->user);

    }

        //for change password
    public function changePassword($id){
        $this->user = $this->user->find($id);
        if(!$this->user){
            request()->session()->flash('error','User not found.');
            return redirect()->route('user.index');
        }

        return view('admin.change-pwd')->with('user_data',$this->user);

    }

    //for upload change-password in the database
    public function submitChangePassword(Request $request, $id){
        $this->user = $this->user->find($id);
        if(!$this->user){
            request()->session()->flash('error','User not found.');
            return redirect()->route('user.index');
        }

        $request->validate([
            'password'=>'required|string|min:8|confirmed'
        ]);

        $data = $request->all();
        $data['password'] = Hash::make($request->password);
        $this->user->fill($data);
        $status = $this->user->save();
        if($status){
            //mail
            request()->session()->flash('success','Password change successfully.');
        }else{
            request()->session()->flash('error','Sorry! There was problem while changing the password.');
        }

        return redirect()->route('user.index');


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->user = $this->user->find($id);
        if(!$this->user){
            request()->session()->flash('error','User not found.');
            return redirect()->route('user.index');
        }

        $rules = array(
            'name' => 'required|string',
            'role' => 'required|in:user,seller',
            'status' => 'required|in:active,inactive'
        );
        $request->validate($rules);
        $data = $request->all();

        $this->user->fill($data);
        $success = $this->user->save();

        if($success){
            //for send mail -- to = $this->user->email
            request()->session()->flash('success','User updated successfully');
        }else{
            request()->session()->flash('error','Sorry! There was problem while updating the user');
        }

        return redirect()->route('user.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user = $this->user->find($id);
        if(!$this->user){
            request()->session()->flash('error','User not found.');
            return redirect()->route('user.index');
        }

        $del = $this->user->delete();
        if($del){
            request()->session()->flash('success','User deleted Successfully.');
        }else{
            request()->session()->flash('error','Sorry! User dose not delete at this movement.');
        }
        return redirect()->route('user.index');

    }


}
