<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductReview;
use App\User;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Null_;

class ProductController extends Controller
{
    protected $category = null;
    protected $user = null;
    protected $product = null;
    protected $review = null;

    public function __construct(Category $category, User $user, Product $product,ProductReview $review)
    {
        $this->category = $category;
        $this->user= $user;
        $this->product= $product;
        $this->review = $review;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->product = $this->product
            ->with(['cat_info','sub_cat_info','seller_info'])      // define in product model
            ->get();
        return view('admin.product-list')->with('product_data',$this->product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $seller_data = $this->user->where('role','seller')->orderBy('name','ASC')->pluck('name','id');

        $all_parents = $this->category->where('parent_id',Null)->orderBy('title','ASC')->pluck('title','id');
        return view('admin.product-form')
            ->with('parent_data',$all_parents)     //parent_data is defined in category column
            ->with('seller_data',$seller_data);    //seller_data is defined in Seller column
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = $this->product->getRules();
        $request->validate($rules);

        $data = $request->except(['image','rel_image']);

        $data['added_by'] = $request->user()->id;
        $data['slug'] = $this->product->getSlug($request->title);

//        //for actual_cost
        $price = $request->price;
        $price = $price - (($price * $request->discount)/100);
        $price = $price + $request->delivery_charge;
        $data['actual_cost'] = $price;
        $data['is_featured'] = $request->input('is_featured',0);



        if($request->image){
            $image_name = uploadImage($request->image,'product',env('PRODUCT_THUMB_SIZE','200x200'));
            if($image_name){
                $data['image'] = $image_name;
            }
        }

        $this->product->fill($data);
        $success = $this->product->save();
        if($success){

//            for multiple image upload
            if($request->rel_image){
                foreach ($request->rel_image as $related_images){
                    $img_name = uploadImage($related_images,'product',env('PRODUCT_THUMB_SIZE','200x200'));
                    if($img_name){
                        $product_image = new ProductImage();
                        $product_image->fill([
                            'product_id'=>$this->product->id,
                            'image_name'=>$img_name
                        ]);
                        $product_image->save();
                    }
                }
            }

            request()->session()->flash('success','Product Added Successfully.');
        }else{
            request()->session()->flash('error','Sorry! There was proble while adding product');
        }

        return redirect()->route('product.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $this->product = $this->product
            ->with(['images','cat_info','sub_cat_info','related_products','reviews'])
            ->where('slug',$slug)
            ->where('status','active')
            ->firstOrFail();

        $reviewed = false;

        //for give permission to comment only one review for a user
        if(auth()->user()){
            $count = $this->review->where('product_id',$this->product->id)->where('user_id',request()->user()->id)->count();
            if($count > 0){
                $reviewed = true;
            }
        }


        //for show number of selected quantity of add to cart
        $qty = 0;
        if(session('cart')){
            foreach (session('cart') as $cart_items){
                if($this->product->id == $cart_items['id']){
                    $qty = $cart_items['quantity'];
                    break;
                }
            }
        }


        return view('home.product-detail')
            ->with('reviewed',$reviewed)
            ->with('quantity',$qty)
            ->with('product_detail',$this->product);
    }

    // for save review into database
    public function submitReview(Request $request){
        $this->product = $this->product->where('slug',$request->slug)->firstOrFail();
//        dd($request->all());

        $request->validate([
            'rating' => 'required|numeric|min:0|max:5',
            'review' => 'nullable|string'
        ]);

        $data = $request->all();
        $data['product_id'] = $this->product->id;
        $data['user_id'] = $request->user()->id;
        $data['status'] = 'active';

        $this->review->fill($data);
        $status = $this->review->save();
        if ($status){
            request()->session()->flash('success','Thank You For Your Review.');
        }else{
            request()->session()->flash('error','Sorry! There was problem while submiting your review.');
        }

        return redirect()->route('product-detail',$this->product->slug);

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->product = $this->product->with('images')->find($id);         //images is defined in Models/Product.php
        if(!$this->product){
            request()->session()->flash('error','Sorry! There is problem while deleting the product.');
            return  redirect()->route('product.index');
        }


        $seller_data = $this->user->where('role','seller')->orderBy('name','ASC')->pluck('name','id');

        $all_parents = $this->category->where('parent_id',Null)->orderBy('title','ASC')->pluck('title','id');
        return view('admin.product-form')
            ->with('product_data',$this->product)
            ->with('parent_data',$all_parents)     //parent_data is defined in category column
            ->with('seller_data',$seller_data);    //seller_data is defined in Seller column


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->product = $this->product->with('images')->find($id);         //images is defined in Models/Product.php
        if(!$this->product){
            request()->session()->flash('error','Sorry! There is problem while deleting the product.');
            return  redirect()->route('product.index');
        }


        $rules = $this->product->getRules('update');
        $request->validate($rules);

        $data = $request->except(['image','rel_image']);

//        //for actual_cost
        $price = $request->price;
        $price = $price - (($price * $request->discount)/100);
        $price = $price + $request->delivery_charge;
        $data['actual_cost'] = $price;
        $data['is_featured'] = $request->input('is_featured',0);

        if($request->image){
            $image_name = uploadImage($request->image,'product',env('PRODUCT_THUMB_SIZE','200x200'));
            if($image_name){
                $data['image'] = $image_name;

                deleteImage($this->product->image,'product');          //delete old image while uploading new image
            }
        }

        $this->product->fill($data);
        $success = $this->product->save();
        if($success){

//            for multiple image upload
            if($request->rel_image){
                foreach ($request->rel_image as $related_images){
                    $img_name = uploadImage($related_images,'product',env('PRODUCT_THUMB_SIZE','200x200'));
                    if($img_name){
                        $product_image = new ProductImage();
                        $product_image->fill([
                            'product_id'=>$this->product->id,
                            'image_name'=>$img_name
                        ]);
                        $product_image->save();
                    }
                }
            }

            //for delete related image
            if(isset($request->del_image) && !empty($request->del_image)){
                foreach ($request->del_image as $del_image){
                    $prod_image = new ProductImage();
                    $prod_image = $prod_image->where('image_name',$del_image)->first();
                    $success = $prod_image->delete();
                    if($success){
                        deleteImage($del_image,'product');
                    }
                }
            }

            request()->session()->flash('success','Product Updated Successfully.');
        }else{
            request()->session()->flash('error','Sorry! There was proble while updating product');
        }

        return redirect()->route('product.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->product = $this->product->with('images')->find($id);         //images is defined in Models/Product.php
        if(!$this->product){
            request()->session()->flash('error','Sorry! There is problem while deleting the product.');
            return  redirect()->route('product.index');
        }

        $rel_image = $this->product->images;
        $image = $this->product->image;

        $del = $this->product->delete();

        if($del){
            deleteImage($image, 'product');     //delete single image
            foreach ($rel_image as $del_image){
                deleteImage($del_image->image_name, 'product');
            }
            request()->session()->flash('success','Product deleted Successfully.');
        }else{
            request()->session()->flash('error','sorry there is problem while deleting product');
        }

        return redirect()->route('product.index');

    }



    public function getAllProducts(){
        $this->product = $this->product->where('status','active')->orderBy('id','DESC')->paginate(5);
        $this->category = $this->category->where('parent_id',null)->where('status','active')->orderBy('id','DESC')->get();
        return view('home.product-list')
            ->with('category',$this->category)
            ->with('product_all',$this->product);
    }

    public function getAllFeaturedProducts(){
        $this->product = $this->product->where('status','active')->where('is_featured',1)->orderBy('id','DESC')->paginate(40);
        $this->category = $this->category->where('parent_id',null)->where('status','active')->orderBy('id','DESC')->get();
        return view('home.product-list')
            ->with('category',$this->category)
            ->with('product_all',$this->product);
    }

}
