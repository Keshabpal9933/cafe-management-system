<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $category = null;
    protected $product = null;
    public function __construct(Category $category,Product $product)
    {
        $this->category = $category;
        $this->product = $product;
    }


    // for fetch and show sub-category in product-form
    public function getChildByParent(Request $request){
        $child_cats= $this->category->where('parent_id',$request->cat_id)->get();
        if($child_cats->count() > 0){
            return response()->json(['data'=>$child_cats,'status'=>true]);
        }else{
            return response()->json(['data'=>$child_cats,'status'=>false]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->category = $this->category->with('child_cats')->where('parent_id',Null)->get();
        return view('admin.category-list')->with('category_data',$this->category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $all_parents = $this->category->where('parent_id',Null)->orderBy('title','ASC')->pluck('title','id');

        return view('admin.category-form')->with('parent_data',$all_parents);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = $this->category->getRules();
        $request->validate($rules);

        $data = $request->except('image');
        $data['slug'] = $this->category->getSlug($request->title);    //make slug in Models/Category.php
        $data['added_by'] = $request->user()->id;    //append foreign-key

        if ($request->image){
            $image_name = uploadImage($request->image,'category', env('CATEGORY_THUMB_SIZE','1500x300'));
            if($image_name){
                $data['image'] = $image_name;
            }
        }

        $this->category->fill($data);
        $status = $this->category->save();

        if($status){
            $request->session()->flash('success','Category added Successfully.');
        }else{
            $request->session()->flash('error','Sorry! There was a problem while adding the category.');
        }

        return redirect()->route('category.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->category = $this->category->find($id);
        if(!$this->category){
            request()->session()->flash('error','Category does not exists');
            return redirect()->route('category.index');
        }

        $all_parents = $this->category->where('parent_id',Null)->orderBy('title','ASC')->pluck('title','id');
        return view('admin.category-form')->with('parent_data',$all_parents)->with('category_data',$this->category);



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->category = $this->category->find($id);
        if(!$this->category){
            request()->session()->flash('error','Category does not exists');
            return redirect()->route('category.index');
        }


        $rules = $this->category->getRules();
        $request->validate($rules);

        $data = $request->except('image');

        if ($request->image){
            $image_name = uploadImage($request->image,'category', env('CATEGORY_THUMB_SIZE','1500x300'));
            if($image_name){
                $data['image'] = $image_name;
            }
        }

        $this->category->fill($data);
        $status = $this->category->save();

        if($status){
            $request->session()->flash('success','Category updated Successfully.');
        }else{
            $request->session()->flash('error','Sorry! There was a problem while updating the category.');
        }

        return redirect()->route('category.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->category = $this->category->find($id);
        if(!$this->category){
            request()->session()->flash('error','Category does not exists or has been already deleted');
            return redirect()->route('category.index');
        }

        $image = $this->category->image;
        $del = $this->category->delete();
        if($del){
            if($image != null){
            deleteImage($image,'category');
            }
            request()->session()->flash('success','Category deleted Successfully.');
        }else{
            request()->session()->flash('error','Sorry! There was a problem while deleting the category.');
        }
        return redirect()->route('category.index');

    }


    // for product-list by category
    public function getAllProductsByCategory(Request $request){
        $this->category = $this->category->where('slug',$request->slug)->where('status','active')->firstOrFail();
        $this->product = $this->product->where('cat_id',$this->category->id)->where('status','active')->orderBy('id','DESC')->paginate(40);
        return view('home.product-list-cat')
            ->with('cat_info',$this->category)
            ->with('product_all',$this->product);
    }

    public function getAllProductsByChildCategory(Request $request){
        $this->category = $this->category->where('slug',$request->slug)->where('status','active')->firstOrFail();
        $this->product = $this->product->where('sub_cat_id',$this->category->id)->where('status','active')->orderBy('id','DESC')->paginate(40);
        return view('home.product-list-cat')
            ->with('cat_info',$this->category)
            ->with('product_all',$this->product);
    }








}
