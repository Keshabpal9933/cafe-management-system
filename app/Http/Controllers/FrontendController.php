<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Slider;
use Illuminate\Http\Request;

class FrontendController extends Controller
{

    protected $slider = null;
    protected $category = null;
    protected $product = null;

    public function __construct(Slider $slider, Category $category, Product $product)
    {
        $this->slider = $slider;
        $this->category = $category;
        $this->product = $product;
    }


    public function index(){

        $this->slider = $this->slider->where('status','active')->orderBy('id','DESC')->limit(5)->get();
        $this->category = $this->category->where('parent_id',null)->where('status','active')->orderBy('id','DESC')->get();
        $this->product = $this->product->where('is_featured',1)->where('status','active')->orderBy('id','DESC')->limit(32)->get();
        return view('home.index')
            ->with('product_all',$this->product)
            ->with('category',$this->category)
            ->with('slider',$this->slider);
    }

}
