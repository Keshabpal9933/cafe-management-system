<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $page_list = array(
            array(
                'title'=>'About Us',
                'summary'=>'This is about us page summary',
                'slug'=> \Str::slug('About Us'),
            ),

            array(
                'title'=>'Privacy policy',
                'summary'=>'This is Privacy policy page ',
                'slug'=> \Str::slug('Privacy policy'),
            ),

            array(
                'title'=>'Term & Condition',
                'summary'=>'This is Term & Condition page ',
                'slug'=> \Str::slug('Term & Condition'),
            ),

            array(
                'title'=>'Return policy',
                'summary'=>'This is Return policy page ',
                'slug'=> \Str::slug('Return policy'),
            ),

            array(
                'title'=>'Contact us',
                'summary'=>'This is Contact us page ',
                'slug'=> \Str::slug('Contact us'),
            ),

            array(
                'title'=>'Help & FAQ',
                'summary'=>'This is Help & FAQ page ',
                'slug'=> \Str::slug('Help & FAQ'),
            )

        );

        DB::table('pages')->insert($page_list);

    }
}
