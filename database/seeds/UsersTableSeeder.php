<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_list = array(
            array(
                'name' => 'Admin User',
                'email' => 'admin@cafe.com',
                'password' => \Illuminate\Support\Facades\Hash::make('admin1234'),
                'role' => 'admin',
                'status' => 'active'
            ),

            array(
                'name' => 'Customer One',
                'email' => 'user@cafe.com',
                'password' => \Illuminate\Support\Facades\Hash::make('user1234'),
                'role' => 'user',
                'status' => 'active'
            )
        );
        DB::table('users')->insert($user_list);
    }
}
